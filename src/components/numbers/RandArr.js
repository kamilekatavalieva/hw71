import React from 'react';

export const randArr = (size) => {
    const array = [];
    for (let i = 0; i < size; i++) {
        while (true) {
            const number = Math.round(Math.random() * (36 + 1 - 5) + 5);
            if(array.includes(number)) {
                continue;
            }
            array.push(number);
            break;
        }
    }
    let arrayNum = Array.from(array);
    return arrayNum.sort(function (a,b) {
        return a-b;
    });
}